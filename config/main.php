<?php
return [
    'components' => [
        'router' => [
            'class' => \Core\Router::class,
        ],
        Core\DB\Db::class => [
            'factory' => \Core\DB\DbFactory::class,
            'dependencies' => [
                'host' => 'localhost',
                'port' => 3306,
                'user' => 'root',
                'pass' => 'malish31',
                'dbname' => 'mvc',
            ]
        ],
        'logger' => [
            'class' => \Core\Logger::class,
        ],
        'api' => [
            'class' => App\Components\Api::class,
            'dependencies' => [
                'request' => App\Components\Request::class,
                'guzzle' => \Psr\Http\Client\ClientInterface::class,
            ]
        ],
        App\Components\Request::class => [
            'class' => App\Components\Request::class,
            'dependencies' => []
        ],
        \Psr\Http\Client\ClientInterface::class => [
            'class' => \GuzzleHttp\Client::class,
            'dependencies' => []
        ]
    ],
    'aliases' => [
        \Core\Logger::class => 'logger',
        \Core\Router::class => 'router',
        \App\Components\Api::class => 'api'
    ]
];