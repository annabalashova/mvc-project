<?php

use Core\Application;
use Core\Logger;

ini_set('display_errors', '1');
//error_reporting(E_ALL & ~E_USER_ERROR);

include '../vendor/autoload.php';

/*$logger = new Logger();
$errorHandler = new \Core\ErrorHandler($logger);
set_error_handler([$errorHandler, 'errHandler']);*/

$config = include '../config/main.php';

$app = Application::getInstance();
$app->configure($config);

$app->run();
