<?php


namespace App\Models;


use Core\contracts\ObserverInterface;
use Core\DB\Model;

class Users extends Model
{
    protected static function tableName()
    {
        return 'users';
    }

    protected function setAddress($name, $valya)
    {
        $this->attributes[$name] = json_encode($valya);
    }

    protected function getAddress($name)
    {
        if ((array_key_exists($name, $this->attributes))) {
            return json_decode($this->attributes[$name]);
        }
    }

}