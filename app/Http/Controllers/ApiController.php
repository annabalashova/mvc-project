<?php


namespace App\Http\Controllers;


use App\Components\Api;
use Core\Application;
use App\Components\Request;
use GuzzleHttp\Client;

class ApiController
{
    public function IndexAction()
    {
        $api = Application::getInstance()->get('api');
        $request = Application::getInstance()->get(Request::class);
        $api->sendRequest();
    }

    public function getUsersAction()
    {
        $api = Application::getInstance()->get('api');
        $client = Application::getInstance()->get(Client::class);
        print_r(json_decode($api->getUsers()->getBody(), true, 512));
    }

}