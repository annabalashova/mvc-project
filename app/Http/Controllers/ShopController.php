<?php
namespace App\Http\Controllers;

use Core\Request;

class ShopController
{
    public function viewAction($id, Request $request)
    {
        echo 'I am viewAction of ShopController' . '<br>' ;
        echo  "I have param $id " . '<br>';
        $request->test();
    }

    public function IndexAction()
    {
        echo 'ShopIndex';
    }

    public function AdminAction()
    {
        echo 'ShopAdmin';
    }

}