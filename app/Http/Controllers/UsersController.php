<?php


namespace App\Http\Controllers;


use App\Models\Users;
use Core\contracts\ObserverManager;

class UsersController
{
    protected function attachEvents($user, $event = '*')
    {
        $observerManager = new ObserverManager();

        $userListener = new \Core\Mail\Mailer();

        $user->setObserverManager($observerManager);
        $observerManager->attach($userListener, $event);
    }

    public function ReadOneAction($id)
    {
        $user = Users::find()->where(['id' => $id])->one();
        $this->attachEvents($user);
        if ($user) {
            $user->callReadNotify($id);
        }
        print_r($user);
    }

    public function ReadAllAction()
    {
        $users = Users::find()->all();
        $this->attachEvents($users[0]);
        if ($users) {
            $users[0]->callReadNotify('all', count($users));
        }
        print_r($users);
    }

    public function CreateAction()
    {
        if (!empty($_GET)) {
            $user = new Users();
            $user->email = htmlspecialchars(strip_tags($_GET['email']));
            $user->first_name = htmlspecialchars(strip_tags($_GET['first_name']));
            $user->last_name = htmlspecialchars(strip_tags($_GET['last_name']));
            $user->password = htmlspecialchars(strip_tags($_GET['password']));
            $user->address = [
                "city" => "Kyiv",
                "street" => "Sribnokilska",
                "house" => 1,
                "room" => 203
            ];

            $this->attachEvents($user, 'create');
            /*$observerManager = new ObserverManager();

            $userListener = new \Core\Mail\Mailer();

            $user->setObserverManager($observerManager);
            $observerManager->attach($userListener, 'create');*/
            if ($user->save()) {
                echo 'Модель успешно создана' . PHP_EOL;
            }
        }
    }

    public function UpdateAction($id)
    {
        if (!empty($_GET)) {
            $user = new Users();
            $user->id = $id;
            $user->email = htmlspecialchars(strip_tags($_GET['email']));
            $user->first_name = htmlspecialchars(strip_tags($_GET['first_name']));
            $user->last_name = htmlspecialchars(strip_tags($_GET['last_name']));
            $user->password = htmlspecialchars(strip_tags($_GET['password']));

            $this->attachEvents($user);

            if ($user->save()) {
                echo 'Модель успешно обновлена' . PHP_EOL;
            }
        }
    }

    public function DeleteAction($id)
    {
        $user = new Users();
        $user->id = $id;

        $this->attachEvents($user, 'delete');
        /* $observerManager = new ObserverManager();

         $userListener = new \Core\Mail\Mailer();

         $user->setObserverManager($observerManager);
         $observerManager->attach($userListener, 'delete');*/
        if ($user->delete()) {
            echo 'Модель успешно удалена' . PHP_EOL;
        }
    }

}