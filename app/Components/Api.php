<?php


namespace App\Components;


use GuzzleHttp\Client;
use Psr\Http\Client\ClientInterface;

class Api
{
    public $client;
    public $request;

    public function __construct(Request $request, ClientInterface $client)
    {
        $this->request = $request;
        $this->client = $client;

    }

    public function sendRequest()
    {
        echo $this->request->test();
    }

    /* public function addUsers(string $metod, string $url)
     {
         $response = $this->client->request($metod, $url);
         return json_encode($response);
     }*/

    public function getUsers()
    {
        $response = $this->client->request(
            'GET',
            'https://gorest.co.in/public-api/users?access-token=BgTFcyr8_OidjC7EHgsIDwWigIyHd1Gy_LVb');
        return $response;
    }

}