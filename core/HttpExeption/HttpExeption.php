<?php


namespace Core\HttpExeption;


class HttpExeption extends \Exception
{
    public $message = 'Ошибка сервера';
    public $code = '500';
}