<?php


namespace Core\HttpExeption;


class ForbiddenHttpExeption extends HttpExeption
{
    public $message = 'Отказано в доступе';
    public $code = '403';
}