<?php


namespace Core\HttpExeption;


class UnauthorizedHttpExeption extends HttpExeption
{
    public $message = 'Необходима авторизация';
    public $code = '401';
}