<?php


namespace Core\HttpExeption;


class NotFoundHttpExeption extends HttpExeption
{
    public $message = 'Страница не найдена';
    public $code = '404';
}