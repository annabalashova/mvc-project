<?php

namespace Core;

use Core\contracts\ContainerAbstract;
use Core\contracts\RunnableInterface;
use Core\contracts\ContainerInterface;
use Core\contracts\BootstrapInterface;
use Core\DB\Db;
use Core\HttpExeption\ForbiddenHttpExeption;
use Core\HttpExeption\HttpExeption;
use Core\HttpExeption\NotFoundHttpExeption;
use Core\HttpExeption\UnauthorizedHttpExeption;
use Exception;

class Application extends ContainerAbstract implements RunnableInterface, ContainerInterface, BootstrapInterface
{

    public static $instance;



    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getInstance()

    {
        if (is_null(self::$instance)) {

            self::$instance = new self();

        }

        return self::$instance;

    }

    public function run()
    {
        try {
            if ($this->has('router')) {
                $action = $this->get('router')->route();
                if ($action) {
                    call_user_func_array([$action['controller'], $action['action']], $action['param']);
                }
            }
            //везде будет добавлено логирование
        } catch (NotFoundHttpExeption $notFoundHttpExeption) {
            echo $notFoundHttpExeption->getCode() . ':' . $notFoundHttpExeption->getMessage();
        } catch (ForbiddenHttpExeption $forbiddenHttpExeption) {
            echo $forbiddenHttpExeption->getCode() . ':' . $forbiddenHttpExeption->getMessage();
        } catch (UnauthorizedHttpExeption $unauthorizedHttpExeption) {
            echo $unauthorizedHttpExeption->getCode() . ':' . $unauthorizedHttpExeption->getMessage();
        } catch (HttpExeption $httpExeption) {
            echo $httpExeption->getCode() . ':' . $httpExeption->getMessage();
        } catch (Exception $exeption) {
            //$logger = new Logger();
            $this->logger->log('debug', 'Произошла ошибка при роутинге ', ['exeption' => $exeption->getMessage()]);
        }

    }
}