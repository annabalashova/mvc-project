<?php


namespace Core;


use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

class Logger extends AbstractLogger implements LoggerInterface
{

    public function log($level, $message, array $context = array())
    {
        $data = $this->format($level, $message, $context);
        $this->write($data);
    }

    public function write($data)
    {
        file_put_contents('/home/anna/www/mvc-project.loc/storage/logs/app.log', $data, FILE_APPEND);

    }

    private function format($level, $message, $context = [])
    {
        return date('Y-m-d H-i-s') . $level . $message . json_encode($context) . PHP_EOL;
    }


}
