<?php


namespace Core\DB;

use Core\Application;
use Core\contracts\ObserverManager;

abstract class Model implements ActiveRecordInterface
{
    protected $attributes = [];

    protected $date;
    /**
     * @var ObserverManager
     */
    public $observerManager;

    public function __construct()
    {
    }

    public function setObserverManager(ObserverManager $observerManager)
    {
        $this->observerManager = $observerManager;
    }

    public function __set($name, $value)
    {
        $setter = 'set' . str_replace('_', '', ucwords($name, '_'));
        if (method_exists(static::class, $setter))
            static::$setter($name, $value);
        elseif ($name === 'password') {
            $this->attributes[$name] = password_hash($value, PASSWORD_DEFAULT);
        } else {
            $this->attributes[$name] = $value;
        }
    }

    public function __get($name)
    {
        $getter = 'get' . str_replace('_', '', ucwords($name, '_'));
        if (method_exists(static::class, $getter))
            static::$getter($name);
        elseif ((array_key_exists($name, $this->attributes))) {
            return $this->attributes[$name];
        }

        return null;
    }

    public function __isset($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    abstract static protected function tableName();

    public static function find($condition = [])
    {
        /** @var QueryBuilder $builder */
        $builder = new QueryBuilder();
        $builder->setModel(static::class);
        $builder->table(static::tableName());
        if ($condition) {
            $builder->where($condition);
        }
        return $builder;
    }

    public function callReadNotify($id, $count = 1)
    {

        $this->observerManager->notify('read', ['id' => $id, 'count' => $count]);

    }

    public static function read($id)
    {
        /** @var QueryBuilder $builder */
        $builder = new QueryBuilder();
        $builder->setModel(static::class);
        $model = $builder->table(static::tableName())->where(['id' => $id])->one();
        return $model;
    }

    public function save()
    {
        if ($this->exists()) {
            return $this->update();
        } else {
            return $this->create();
        }

    }

    public function delete()
    {
        $this->observerManager->notify('delete', ['id' => $this->id]);

        $query = "DELETE FROM " . $this->tableName() . " WHERE id = " . $this->id;
        return Application::getInstance()->get(Db::class)->query($query);
    }

    public function create()
    {
        $this->observerManager->notify('create', $this->attributes);
        $this->attributes['created_at'] = $this->setDate();
        foreach ($this->attributes as $key => $value) {
            $attributes[$key] = "'" . $value . "'";
        }
        $query = "INSERT INTO " . static::tableName() . " (" . implode(',', array_keys($attributes)) . ")"
            . " VALUES (" . implode(',', $attributes) . ")";
        Application::getInstance()->get(Db::class)->query($query);
        if ($id = Application::getInstance()->get(Db::class)->insert($query)) {
            $this->id = $id;
        }
        return $this;
    }

    public function update()
    {
        $this->observerManager->notify('update', $this->attributes);
        $this->attributes['updated_at'] = $this->setDate();
        foreach ($this->attributes as $key => $value) {
            $attributes[$key] = $key . "='" . $value . "'";
        }
        $query = "UPDATE " . static::tableName() . " SET " . implode(',', $attributes) . " WHERE id = " . $this->id;
        Application::getInstance()->get(Db::class)->query($query);

        return $this;
    }

    /**
     * @return bool
     * @todo Сделать проверку реального существования записи в базе данных
     */
    protected function exists()
    {
        return isset($this->id);
    }

    protected function fill($attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public static function createModel($attributes)
    {
        $model = new static();
        $model->fill($attributes);
        return $model;
    }

    public static function createModels($rows)
    {
        $models = [];
        foreach ($rows as $row) {
            $models[] = static::createModel($row);
        }
        return $models;
    }

    protected function setDate()
    {
        $this->date = new \DateTime();
        return $this->date->format('Y-m-d H:i:s');
    }
}