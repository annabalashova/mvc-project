<?php


namespace Core\DB;


use Core\contracts\ComponentAbstract;
use Core\contracts\ComponentFactoryAbstract;
use Core\contracts\ContainerAbstract;

class DbFactory extends ComponentFactoryAbstract
{
    /**
     * @return ComponentAbstract
     * ContainerInterface $app
     */
    public function createConcreteInstance(): ComponentAbstract
    {
        $db = Db::getInstance();
       if (!$db->isConfigured()) {
            $config = $this->app->config(ContainerAbstract::KEY_COMPONENTS)[Db::class];
            $dependencies = $config['dependencies'];
            $db->configure($dependencies);
       }
        return $db;

    }
}