<?php

namespace Core\DB;


use Core\contracts\ComponentAbstract;
use Core\contracts\ConfigurableInterface;

class Db extends ComponentAbstract implements ConfigurableInterface
{
    protected $host;

    protected $port;

    protected $user;

    protected $pass;

    protected $dbname;

    protected $configured;

    protected $connection;

    protected static $instance;

    protected function __construct() {}

    protected function __clone() {}

    public function __destruct()
    {
        $this->getConnection()->close();
    }

    static public function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function configure(array $config)
    {
        foreach ($config as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
        $this->configured = true;
    }

    protected function getConnection()
    {
        if (!$this->connection) {
            $this->connection = new \mysqli($this->host, $this->user, $this->pass, $this->dbname, $this->port);
        }

        return $this->connection;
    }

    public function query($sql)
    {
        $response = $this->getConnection()->query($sql);

        return $response;
    }

    public function insert($sql)
    {
        if ($this->getConnection()->query($sql)) {
            return $this->getConnection()->insert_id;
        }

        return false;
    }

    public function isConfigured()
    {
        return $this->configured;
    }

    public function bootstrap()
    {
        // TODO: Implement bootstrap() method.
    }
}
