<?php
/**
 * Файл содержит класс QueryBuilder
 *
 * @version 1.0
 *
 * @author Аnna Balashova
 */

namespace Core\DB;



use Core\Application;
use Core\contracts\QueryBuilderInterface;
use Exception;
use mysqli;
use \Core\DB\Db;

/**
 * Class QueryBuilder
 *
 * Формирует строку запроса в базу данных, производит запрос и получает результат в виде массива
 *
 * При написании  был применен паттерн Build(Строитель)
 *
 * @package Core\DB
 */
class QueryBuilder implements QueryBuilderInterface
{
    /**
     * @var \Core\DB\Db содержит экземпляр данного класса
     */
    protected $db;

    /**
     * @var array содержит массив с наименованиями колонок для выборки, определено значение по умолчанию
     */
    public $columns = ['*'];
    /**
     * @var array  содержит массив данных с условиями для выборки (оператор WHERE)
     */
    public $conditions = [];
    /**
     * @var string содержит строку с названием таблицы
     */
    public $table;
    /**
     * @var int содержит  число, ограничивающее количество строк результата выборки
     */
    public $limit;
    /**
     * @var int содержит число, определяющее количество строк смещения результата
     */
    public $offset;
    /**
     * @var array содержит массив с данными для сортировки
     */
    public $order = [];

    protected $asArray;

    protected $modelClass;

    public function asArray()
    {
        $this->asArray = true;

        return $this;
    }

    public function setModel($modelClass)
    {
        $this->modelClass = $modelClass;

        return $this;
    }


    /**
     * QueryBuilder constructor.
     */
    public function __construct()
    {
        $this->db = Application::getInstance()->get(Db::class);
    }

    /**
     * Принимает и записывает в переменную данные  для выбору колонок (оператор SELECT)
     *
     * @param array $columns принимает массив с наименованиями колонок для выборки
     *
     * @return QueryBuilderInterface возвращает себя как экземпляр класса
     */
    public function select($columns): QueryBuilderInterface
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * Принимает и записывает в переменную данные с условиями для выборки (оператор WHERE)
     *
     * @param array $conditions принимает индексированный массив [колонка => значение]
     *
     * @return QueryBuilderInterface возвращает себя как экземпляр класса
     */
    public function where($conditions): QueryBuilderInterface
    {
        $this->conditions = $conditions;
        return $this;
    }

    /**
     * Принимает и записывает в переменную наименвание таблицы для выборки(оператор FROM)

     * @param string $table строка с названием таблицы
     *
     * @return QueryBuilderInterface возвращает себя как экземпляр класса
     */
    public function table($table): QueryBuilderInterface
    {
        $this->table = $table;
        return $this;
    }

    /**
     * Принимает и записывает в переменную число, определяющее ограничение результата определенным количеством
       строк(оператор LIMIT)
     *
     * @param int $limit число, определяющее ограничение результата
     *
     * @return QueryBuilderInterface возвращает себя как экземпляр класса
     */
    public function limit($limit): QueryBuilderInterface
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * Принимает и записывает в переменную число, определяющее смещение результата на определенное количество
    строк(оператор OFFSET)
     *
     * @param int $offset число, определяющее  количество строк, которое пропускается при выдаче результата
     *
     * @return QueryBuilderInterface возвращает себя как экземпляр класса
     */
    public function offset($offset): QueryBuilderInterface
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * Принимает и записывает в переменную данные для сортировки
     *
     * @param array $order принимает индексированный массив [колонка => порядок сортировки]
     *
     * @return QueryBuilderInterface возвращает себя как экземпляр класса
     */
    public function order($order): QueryBuilderInterface
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Метод формирует строку запроса в БД, используя свойства класса
     *
     * @return string возврашает строку запроса
     *
     * @throws Exception формирует ошибку, в случае, если таблица(обязательный элемент запроса) не задана
     */
    public function build(): string
    {
        if (empty($this->table)) {
            throw new \Exception('table is required');
        }

        $result = "SELECT " . implode(', ', $this->columns);

        $result .= " FROM " . $this->table;

        if ($this->conditions) {
            $result .= " WHERE ";
            foreach ($this->conditions as $key => $value) {
                $where[] = $key . " = " . $value;
            }
            $result .= implode(" AND ", $where);
        }

        if ($this->order) {

            $result .= " ORDER BY ";

            foreach ($this->order as $key => $value) {
                $order[] = $key . " " . $value;
            }
            $result .= implode(", ", $order);
        }

        if ($this->limit) {
            $result .= " LIMIT " . $this->limit;

            if ($this->offset) {
                $result .= " OFFSET " . $this->offset;
            }
        }

        $this->reset();

        return $result;
    }

    /**
     * Метод вызвает метод своего класса Build(), формирует строку, с помощью метода sendQuery() класса Db
     производит запрос и получает результат в виде ассоциативного массива
     *
     * @return array|null в случае успеха возвращается одна строка из результирующего набора в виде ассоциативного
     массива
     *
     * @throws Exception
     */
    public function one()
    {
        $this->limit = 1;
        $sql = $this->build();
        $response = $this->db->query($sql);

        $row = mysqli_fetch_assoc($response);

        if (
            !$this->asArray
            && $this->modelClass
            && is_a($this->modelClass, ActiveRecordInterface::class, true)
        ) {
            return $this->modelClass::createModel($row);
        }

        return $row;
    }

    public function all()
    {
        $sql = $this->build();
        $response = $this->db->query($sql);

        $rows = mysqli_fetch_all($response, MYSQLI_ASSOC);

        if (
            !$this->asArray
            && $this->modelClass
            && is_a($this->modelClass, ActiveRecordInterface::class, true)
        ) {
            return $this->modelClass::createModels($rows);
        }

        return $rows;
    }
    /**
     * Обнуляет данные для запроса
     */
    public function reset()
    {
        $this->columns = ['*'];
        $this->conditions = [];
        $this->table = null;
        $this->limit = '';
        $this->offset = '';
        $this->order = [];
    }
}


