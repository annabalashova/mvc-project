<?php


namespace Core;

use Core\HttpExeption\ForbiddenHttpExeption;
use Core\HttpExeption\NotFoundHttpExeption;
use Core\HttpExeption\UnauthorizedHttpExeption;

class Router
{

    public function route()
    {
        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $parts = explode('/', $url);

        $controllerName = 'App\\Http\\Controllers\\IndexController';
        if (!empty($parts[1])) {
            $controllerName = 'App\\Http\\Controllers\\' . ucfirst($parts[1]) . 'Controller';
            if (!class_exists($controllerName)) {
                // throw new \Exception('Controller not found');
                throw new NotFoundHttpExeption();
            }
            if ($controllerName == 'App\\Http\\Controllers\\ServerController') {
                throw new ForbiddenHttpExeption();
            }

        }

        $controller = new $controllerName();
        $action = 'IndexAction';
        if (!empty($parts[2])) {
            $action = $parts[2] . 'Action';
            if (!method_exists($controller, $action)) {
                throw new \Exception('Action not found');
            }
            if ($action == 'adminAction') {
                throw new UnauthorizedHttpExeption();
            }
        }

        $paramsPart = (!empty($_GET)) ? $_GET : [];

        $reflectionMethod = new \ReflectionMethod($controller, $action);
        $dependencies = Application::getInstance()->resolveDependencies($reflectionMethod, $paramsPart);

        /*$sendParams = [];
        foreach ($reflectionMethod->getParameters() as $parameter) {
            $parameterName = $parameter->getName();
            if (array_key_exists($parameterName, $paramsPart)) {
                $sendParams[$parameterName] = $paramsPart[$parameterName];
            } else {
                trigger_error('Обязательный параметр не передан', E_USER_ERROR);
            }
        }*/

        /*$params1 = [];
 for ($i = 3, $j = 0; $i < count($parts); $i++, $j++) {
     $params[$j][$parts[$i]] = $parts[++$i];
 }*/
        $route['controller'] = $controller;
        $route['action'] = $action;
        $route['param'] = $dependencies;
        return $route;
    }
}