<?php


namespace Core\Mail;


use Core\contracts\ObserverInterface;
use PHPMailer\PHPMailer\PHPMailer;

class Mailer implements ObserverInterface
{

    public function listen($event, $data = [])
    {
        echo 'Mailer: произошло событие ' . $event . ' с данными ' . json_encode($data) . PHP_EOL;
        $this->sendMail();
    }

    protected function sendMail()
    {
        $mail = new  PHPMailer();
        $mail->SMTPDebug = 3;
//Задаем для PHPMailer использовать SMTP.
        $mail->isSMTP();
//Устанавливаем имя хоста SMTP
        $mail->Host = "smtp.gmail.com";
//Устанавливаем для этого параметра значение true, если хост SMTP требует аутентификации для отправки почты
        $mail->SMTPAuth = true;
//Предоставляем имя пользователя и пароль
        $mail->Username = "balashovaa508@gmail.com";
        $mail->Password = "malish31";
//Если для SMTP требует шифрование TLS, устанавливаем его
        $mail->SMTPSecure = "tls";
//Устанавливаем порт TCP для подключения
        $mail->Port = 587;
        $mail->From = "balashovaa508@gmail.com";
        $mail->FromName = "robot";
        $mail->addAddress("balashovaa508@gmail.com", "Anna");
        $mail->addReplyTo("balashovaa508@gmail.com");
        $mail->isHTML(true);
        $mail->Subject = "Тема письма";
        $mail->Body = "<i>Тело письма в HTML</i>";
        $mail->AltBody = "Текстовая версия письма";
        if (!$mail->send()) {
            echo "Ошибка: " . $mail->ErrorInfo;
        } else {
            echo "Сообщение успешно отправлено";
        }
    }
}