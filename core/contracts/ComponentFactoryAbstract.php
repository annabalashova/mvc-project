<?php


namespace Core\contracts;


abstract class ComponentFactoryAbstract
{
    protected $app;

    public function __construct(ContainerInterface $app)
    {
        $this->app = $app;
    }

    public function createInstance()
    {
        return $this->createConcreteInstance();
    }

    abstract public function createConcreteInstance();
}