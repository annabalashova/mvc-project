<?php


namespace Core\contracts;


interface ObserverInterface
{
    public function listen($event, $data = []);
}