<?php


namespace Core\contracts;


class ObserverManager implements SubjectInterface
{
    protected $observers = [];

    public function attach(ObserverInterface $observer, $event = '*')
    {
        $events = (array)$event;
        foreach ($events as $value) {
            $this->observers[$value][] = $observer;
        }
    }

    public function detach(ObserverInterface $observer, $event = '*')
    {
        foreach ($this->observers[$event] as $key => $item) {
            if ($observer === $item) {
                unset($this->observers[$key]);
            }
        }
    }

    public function notify(string $event, $data = [])
    {
        $all = $this->observers['*'] ?? [];
        $events = $this->observers[$event] ?? [];
        $observers = array_merge($events, $all);
        foreach ($observers as $observer) {
            $observer->listen($event, $data);
        }
    }
}