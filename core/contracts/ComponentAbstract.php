<?php
namespace Core\contracts;

use Core\contracts\BootstrapInterface;

abstract class ComponentAbstract implements BootstrapInterface
{
    abstract public function bootstrap();

}