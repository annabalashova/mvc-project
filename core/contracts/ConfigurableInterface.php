<?php


namespace Core\contracts;


interface ConfigurableInterface
{
public function configure(array $config);
}