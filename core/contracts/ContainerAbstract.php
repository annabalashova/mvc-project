<?php

namespace Core\contracts;

abstract class ContainerAbstract implements ContainerInterface
{
    protected $components = [];

    protected $config = [];

    public $instances = [];

    public $aliases = [];

    public const KEY_COMPONENTS = 'components';

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __set($name, $value)
    {
        $this->components[$name] = $value;
    }

    public function configure($config)
    {
        $this->config = $config;

        $this->bootstrap();

        $this->getAlias();
    }

    public function bootstrap()
    {
        if (isset($this->config['components']) && is_array($this->config['components'])) {
            foreach ($this->config['components'] as $name => $component) {
                $this->components[$name] = $component;
            }
        }
    }

    protected function createObject($name)
    {
        if (array_key_exists($name, $this->components)) {
            $component = $this->components[$name];
            if (isset($component['factory']) && class_exists($component['factory'])) {
                $factory = new $component['factory']($this);
                $instance = $factory->createInstance();

                return $instance;
            } elseif (is_array($component) && array_key_exists('class', $component)) {
                return $this->resolveService($component);
            }
        }

        throw new \Exception('Can not instantiate object ' . $name);

    }

    public function has($name)
    {
        return array_key_exists($name, $this->components);
    }

    public
    function get($name)
    {
        if ($this->checkComponentOnAlias($name)) {
            $name = $this->aliases[$name];
        }

        if (!array_key_exists($name, $this->instances)) {
            $instance = $this->createObject($name);
            $this->instances[$name] = $instance;
        }

        return $this->instances[$name];
    }

    public function config($name = null)
    {
        if (is_null($name)) {
            return $this->config;
        }

        if (array_key_exists($name, $this->config)) {
            return $this->config[$name];
        }

        return null;
    }

    protected function resolveService($config)
    {
        $reflectionClass = new \ReflectionClass($config['class']);
        $constructor = $reflectionClass->getConstructor();
        if ($constructor === null) {
            return $reflectionClass->newInstance();
        }
        $dependencies = $this->resolveDependencies($constructor, $config);
        return $reflectionClass->newInstanceArgs($dependencies);
    }

    public function resolveDependencies(\ReflectionFunctionAbstract $method, $params = [])
    {
        $dependencies = [];
        foreach ($method->getParameters() as $parameter) {
            $name = $parameter->getName();
            if (array_key_exists($name, $params)) {
                $dependencies[$name] = $params[$name];
            } elseif (isset($params['dependencies']) && array_key_exists($name, $params['dependencies'])) {
                $dependencies[$name] = $this->get($params['dependencies'][$name]);
            } elseif ($parameter->getType() && !$parameter->getType()->isBuiltin()) {
                $dependencies[$name] = $this->get($parameter->getType()->getName());
            }
        }
        return $dependencies;
    }

    protected function getAlias()
    {
        $this->aliases = $this->config['aliases'];
    }

    protected function checkComponentOnAlias($name)
    {
        $this->getAlias();
        if (!array_key_exists($name, $this->components) && array_key_exists($name, $this->aliases)) {
            return true;
        }
        return false;
    }

    public function addAlias($alias, $component)
    {
        if (array_key_exists($component, $this->components)) {
            $this->aliases[$alias] = $component;
        }
    }


}