<?php


namespace Core\contracts;


interface SubjectInterface
{
public function attach(ObserverInterface $observer);

    public function detach(ObserverInterface $observer);

    public function notify(string $event, $data = []);
}