<?php

namespace Core\contracts;

interface RunnableInterface
{
public  function run();
}