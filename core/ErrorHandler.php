<?php

namespace Core;


class ErrorHandler
{
    protected $logger;

    public function errHandler($errNo, $errStr, $errFile, $errLine, $errContext)
    {

        $this->logger->error($errStr, ['context' => $errContext, 'file' => $errFile, 'line' => $errLine ]);
        echo "Произошла ошибка уровня $errNo" . ':' . $errStr;


    }

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

}